import React from 'react';

export default class AddTask extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            taskName: '',
            taskPriority: 'low',
            taskDuration: 0
        };
    }

    render(){
        return (
        <div>
       <input type="text" id="task-name" name="task-name" onChange={this.handleChange} value={this.state.taskName} />
     <input type="text" id="task-priority" name="task-priority" onChange={this.handleChange} value={this.state.taskPriority} />
     <input type="text" id="task-duration" name="task-duration" onChange={this.handleChange} value={this.state.taskDuration} />
    <button value = "add task" onClick ="addTask()"></button>
        </div>
        );
    }

    addTask = () => {
        let task = {...this.state};
        //     let task = {
        //     name: this.state.name,
        //     category: this.state.taskPriority,
        //     price: this.state.taskDuration
        // };
        this.props.taskAdded(task);
    }
}
